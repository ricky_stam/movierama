﻿/// <reference path="_all.js" />

movieRama.dom = (function ($) {

    'use strict'

    var addMovieToDom = function (m) {
        var el = movieRama.templates.mainMovieTmp({ movie: m });
        $('#movieList').append(el);
    }

    var appendMovieDetails = function (detailsElement, element) {
        $(detailsElement).appendTo($(element).find('.main-info')).hide().fadeIn('slow');
    }

    var appendReviews = function (reviewsEl, element) {
        var el = $(element);
        el.animate({ width: "1140px" }, 1000, function () {
            el.append(reviewsEl);
        });
    }

    var showMovieDetails = function (element) {
        var el = $(element);
        el.animate({ width: "1140px" }, 1000, function () {
            el.find('#director-similar-movies, #reviews').show();
        });
        el.data("expanted", true);
    }

    var hideMovieDetails = function (element) {
        var el = $(element);
        el.find('#director-similar-movies, #reviews').hide();
        el.animate({ width: "725px" });
        el.data("expanted", false);
    }

    var updateResultsHeader = function (query, totalRecords) {
        var element;
        if (query) {
            element = movieRama.templates.resultsFoundTemp({ count: totalRecords });
        }
        else {
            element = movieRama.templates.moviesInTheaterTemp({ count: totalRecords });
        }
        $('#results').html(element);
    }

    var clearMovieListAndResults = function () {
        $('#movieList').empty();
        $('#results').empty();
    }

    var showSpinner = function () {
        $('#spinner').show();
        $('body').scrollTop($("#spinner").offset().top + 50);
    }

    var hideSpinner = function () {
        $('#spinner').hide();
    }

    movieRama.appEventService.subscribe(movieRama.appEventService.events.movieAdded, addMovieToDom);
    movieRama.appEventService.subscribe(movieRama.appEventService.events.showSpinner, showSpinner);

    return {
        appendMovieDetails: appendMovieDetails,
        appendReviews: appendReviews,
        hideMovieDetails: hideMovieDetails,
        showMovieDetails: showMovieDetails,
        updateResultsHeader: updateResultsHeader,
        clearMovieListAndResults: clearMovieListAndResults,
        showSpinner: showSpinner,
        hideSpinner: hideSpinner
    }


})(jQuery)