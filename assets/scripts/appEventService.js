﻿/// <reference path="_all.js" />

var movieRama = movieRama || {};

//https://davidwalsh.name/pubsub-javascript

movieRama.appEventService = (function ($) {

    'use strict'

    var events = {};
    var hOP = events.hasOwnProperty;

    return {
        subscribe: function (event, fn) {
            if (!hOP.call(events, event)) {
                events[event] = [];
            }

            var index = events[event].push(fn) - 1;

            return {
                remove: function () {
                    delete events[event][index];
                }
            };
        },

        publish: function (event, data) {
            if (!hOP.call(events, event)) {
                return;
            }
            
            events[event].forEach(function (item) {
                item(data != undefined ? data : {});
            });
        },

        events: {
            loadMoreData: "loadMoreData",
            nextPage: "nextPage",
            movieAdded: "movieAdded",
            showSpinner: "showSpinner",
            hideSpinner:"hideSpinner"
        }

    };

})(jQuery);