﻿/// <reference path="_all.js" />

movieRama.paginationService = (function ($) {

    'use strict'

    var page = 1;
    var pageSize = 10;
    var totalRecords;
    var totalPages;

    var getPaginationSettings = function () {
        return {
            page: page,
            pageSize: pageSize,
            totalRecords: totalRecords,
            totalPages: totalPages
        }
    }

    var resetPage = function () {
        page = 1;
    }

    var setMaxPages = function (maxRecords) {
        var maxPages = Math.ceil(maxRecords / pageSize);
        totalPages = maxPages;
        totalRecords = maxRecords;

        movieRama.dom.updateResultsHeader(query,totalRecords);
    }

    var nextPage = function () {
        page++;
        if(page < totalPages){
            movieRama.appEventService.publish(movieRama.appEventService.events.loadMoreData, page);
        }
    }

    movieRama.appEventService.subscribe(movieRama.appEventService.events.nextPage, nextPage);

    return {
        nextPage: nextPage,
        setMaxPages: setMaxPages,
        getPaginationSettings: getPaginationSettings,
        resetPage: resetPage
    }

})(jQuery)