﻿/// <reference path="_all.js" />

movieRama.model = (function () {

    'use strict'


    return {

        movies: [],

        movie: function (id, poster, title, year, runtime, rating, cast, synopsis) {
            this.Id = id;
            this.Poster = poster;
            this.Title = title;
            this.Year = year;
            this.Runtime = runtime;
            this.Rating = rating;
            this.Cast = [];
            for (var i = 0; i < 3; i++) {
                if (cast && cast.length > 0 && cast[i]) {
                    this.Cast.push(cast[i].name);
                }
            }

            this.Synopsis = synopsis;
        },

        addMovie: function (m) {
            movieRama.model.movies.push(m)
            movieRama.appEventService.publish(movieRama.appEventService.events.movieAdded, m);
        },

        similarMovie:function(title,url){
            this.Title = title;
            this.Url = url;
        },

        review: function (quote, critic,date) {
            this.Quote = quote;
            this.Critic = critic;
            this.Date = date;
        },
    };
})();


