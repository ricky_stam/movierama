﻿/// <reference path="_all.js" />

movieRama.movieTemplateFactory = (function (factory, $, _) {

    'use strict'

    factory.createDirectorsTemp = function (directors) {
        var dilimit = "";
        var directorsString = "";
        directors.forEach(function (d) {
            directorsString += dilimit + d;
            dilimit = ", ";
        })

        return movieRama.templates.directorsTemp({ directors: directorsString });
    }

    factory.createSimilarMoviesTemp = function (similarMovies) {
        var similarMoviesElement = "";
        similarMovies.forEach(function (m) {
            similarMoviesElement += movieRama.templates.similarMoviesTemp({ movie: m });
        })

        return similarMoviesElement;
    }

    factory.createReviewsTemp = function (reviews) {
        var reviewsElement = "";
        reviews.forEach(function (r) {
            reviewsElement += movieRama.templates.reviewTmp({ review: r });
        })

        reviewsElement = movieRama.templates.reviewsContainerTemp({ reviews: reviewsElement });

        return reviewsElement;
    }

    factory.createTemplate = function (type,data) {
        return factory[type](data);
    }

    factory.types = {
        createReviewsTemp: "createReviewsTemp",
        createDirectorsTemp: "createDirectorsTemp",
        createSimilarMoviesTemp: "createSimilarMoviesTemp"
    }

    return {
        types:factory.types,
        createTemplate: factory.createTemplate
    };

})(movieRama.movieTemplateFactory = movieRama.movieTemplateFactory || {},jQuery,_);