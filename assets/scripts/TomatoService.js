﻿/// <reference path="_all.js" />


movieRama.tomatoService = (function ($, _) {

    'use strict'


    var apiKey = "?apikey=qtqep7qydngcc7grk4r4hyd9";
    var baseUrl = "http://api.rottentomatoes.com";

    var query = "";

    var movieSearchEndPoint = _.template(baseUrl + "/api/public/v1.0/movies.json" + apiKey + "&page_limit=" + movieRama.paginationService.getPaginationSettings().pageSize + "&page=<%= page %>" + "&q=<%= query %>");
    var movieListEndPoint = _.template(baseUrl + "/api/public/v1.0/lists/movies/in_theaters.json" + apiKey + "&page_limit=" + movieRama.paginationService.getPaginationSettings().pageSize + "&page=<%= page %>");
    var movieDetailsEndPoint = _.template(baseUrl + "/api/public/v1.0/movies/<%= movieId %>.json" + apiKey);
    var movieReviewsEndPoint = _.template(baseUrl + "/api/public/v1.0/movies/<%= movieId %>/reviews.json" + apiKey);
    var movieSimilarEndPoint = _.template(baseUrl + "/api/public/v1.0/movies/<%= movieId %>/similar.json" + apiKey + "&limit=2");

    var cache = {};

    var ajaxCall = function (url) {
        return $.ajax({
            url: url,
            dataType: 'jsonp'
        })
    }

    var loadMoreMovies = function (page) {
        //if we have a query string
        if (query && cache[query+"_" + page]) {
            return cache[query + "_" + page];
        }
        else if (query && !cache[query + "_" + page]) {
            //show spinner
            movieRama.appEventService.publish(movieRama.appEventService.events.showSpinner);
            cache[query + "_" + page] = ajaxCall(movieSearchEndPoint({ page: page, query: query }));
            return cache[query + "_" + page];
        }

        //if we don't have a query string
        if (!query && cache[page]) {
            return cache[page];
        }
        else if (!query && !cache[page]) {
            //show spinner
            movieRama.appEventService.publish(movieRama.appEventService.events.showSpinner);
            cache[page] = ajaxCall(movieListEndPoint({ page: page }));
            return cache[page];
        }
    }

    var addMovies = function (page) {
        loadMoreMovies(page)
            .then(function (res) {
                movieRama.paginationService.setMaxPages(res.total);
                if (res.movies && res.movies.length > 0) {
                    _.each(res.movies, function (m) {
                        var movie = new movieRama.model.movie(m.id, m.posters.thumbnail, m.title, m.year, m.runtime, m.ratings.audience_score, m.abridged_cast, m.synopsis);
                        movieRama.model.addMovie(movie);
                    });
                }
            }, function (err) {
                console.log(err);
            })
    }

    var getMovieDetails = function (element) {
        var id = $(element).data('movie-id');

        var templateObj = { movieId: id };
        var detailsCall = ajaxCall(movieDetailsEndPoint(templateObj));
        var similarCall = ajaxCall(movieSimilarEndPoint(templateObj));
        var reviewsCall = ajaxCall(movieReviewsEndPoint(templateObj));

        var directorsPromise = $.Deferred();
        var similarMoviesPromise = $.Deferred();
        var reviewsPromise = $.Deferred();

        detailsCall.then(function (res) {
            var directors = [];
            res.abridged_directors.forEach(function (d) {
                directors.push(d.name);
            })
            return directorsPromise.resolve(directors);
        },
            function (err) {
                console.log(err);
                return directorsPromise.reject();
            })

        similarCall.then(function (res) {
            var similarMovies = [];
            res.movies.forEach(function (m) {
                var url = "";
                if (m.links && m.links.alternate) {
                    url = m.links.alternate
                }
                similarMovies.push(new movieRama.model.similarMovie(m.title, url));
            })

            return similarMoviesPromise.resolve(similarMovies);
        },
            function (err) {
                console.log(err);
                return similarMoviesPromise.reject();
            })

        reviewsCall.then(function (res) {
            var reviews = [];

            if (res && res.reviews && res.reviews.length > 0) {
                for (var i = 0; i < 2; i++) {
                    if (res.reviews[i]) {
                        reviews.push(new movieRama.model.review(res.reviews[i].quote, res.reviews[i].critic, res.reviews[i].date))
                    }
                }
            }

            return reviewsPromise.resolve(reviews);
        },
            function (err) {
                console.log(err);
                return reviewsPromise.reject();
            })

        $.when(directorsPromise, similarMoviesPromise, reviewsPromise).then(function (directorData, similarMoviesData, reviewsData) {
            movieRama.templates.appendMovieDetais(directorData, similarMoviesData, reviewsData, element);
        },
            function (err) {
                console.log(err);
            })
    }

    var setQueryParameter = function (searchTerm) {
        query = searchTerm;
    }

    movieRama.appEventService.subscribe(movieRama.appEventService.events.loadMoreData, addMovies)

    return {
        setQueryParameter: setQueryParameter,
        getMovieDetails: getMovieDetails,
        addMovies: addMovies
    }

})(jQuery, _);

