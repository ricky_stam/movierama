﻿/// <reference path="_all.js" />

movieRama.templates = (function ($, _) {

    'use strict'

    return {
        mainMovieTmp: _.template('\
        <div data-movie-id="<%- movie.Id %>" class="movie-container clearfix">\
                <div class="poster">\
                    <img src="<%- movie.Poster %>">\
                </div>\
                <div class="main-info">\
                    <h4><%- movie.Title %></h4>\
                    <p><%- movie.Year %> - <%- movie.Runtime %> Mins - <%- movie.Rating %>/100</p>\
                    <% if(movie.Cast && movie.Cast.length > 0){ %>\
                    <p>Starring <%- movie.Cast %></p>\
                    <% } %>\
                    <p><%- movie.Synopsis %></p>\
                </div>\
        </div>\
        '),

        moviesInTheaterTemp: _.template('<h2><%- count %> Movie(s) in theater this week</h2>'),

        resultsFoundTemp: _.template('<h2><%- count %> Movie(s) found</h2>'),

        reviewsContainerTemp: _.template('\
                            <div id="reviews">\
                                <h4>Latest Reviews</h4>\
                                <div class="reviews"><%= reviews %></div>\
                            </div>\
                               '),

        reviewTmp: _.template('<div><%= review.Quote %> <cite> <%= review.Critic %> <%= review.Date %> </cite></div>'),

        similarMoviesAndDirectorsTemp: _.template('\
                        <div id="director-similar-movies">\
                        <%= directorsHtml %>\
                        <%= similarMoviesHtml %>\
                        </div>\
                        '),

        similarMoviesTemp: _.template('<a class="movie-link" target="_blank" href="<%- movie.Url %>"><%- movie.Title %></a>'),

        directorsTemp: _.template('<p>Directors: <%- directors %></p>'),

        appendMovieDetais: function (directors, similarMovies, reviews, element) {
            //create direcotrs Element
            var directorsEl = movieRama.movieTemplateFactory.createTemplate(movieRama.movieTemplateFactory.types.createDirectorsTemp, directors);

            //create similar movies Element
            if (similarMovies && similarMovies.length > 0) {
                var similarMoviesEl = movieRama.movieTemplateFactory.createTemplate(movieRama.movieTemplateFactory.types.createSimilarMoviesTemp,similarMovies);
            }

            //create reviews Element
            if (reviews && reviews.length > 0) {
                var reviewsEl = movieRama.movieTemplateFactory.createTemplate(movieRama.movieTemplateFactory.types.createReviewsTemp, reviews);
            }

            var detailsElement = movieRama.templates.similarMoviesAndDirectorsTemp({ directorsHtml: directorsEl, similarMoviesHtml: similarMoviesEl })

            movieRama.dom.appendMovieDetails(detailsElement, element);

            if (reviews) {
                movieRama.dom.appendReviews(reviewsEl, element);
            }

            $(element).data("expanted", true);
            $(element).data("hasdata", true);

        }

    }
})(jQuery,_);