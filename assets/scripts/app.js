﻿/// <reference path="_all.js" />

movieRama.app = (function (app,$, _) {

    'use strict'

    app.init = function () {
        movieRama.tomatoService.addMovies(1);
    }

    app.searchMovie = function () {
        movieRama.paginationService.resetPage();
        movieRama.tomatoService.addMovies(1);
    }

    app.clearAppState = function (queryTerm) {
        movieRama.model.movies = [];
        movieRama.dom.clearMovieListAndResults();
        movieRama.paginationService.resetPage();
    }

    app.infiniteScrollFn = function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            //load more data only if the spinner is not visible
            if ($('#spinner').is(':hidden')) {
                movieRama.appEventService.publish(movieRama.appEventService.events.nextPage);
            }

        }
    }

    app.clickSearchFn = function () {
        movieRama.app.clearAppState($('#query').val());
        movieRama.tomatoService.setQueryParameter($('#query').val());
        movieRama.app.searchMovie();
    }

    app.keypressQueryFn = function (e) {
        if (e.which == 13) {
            $('#search').click();
            return false;
        }
    }

    app.keyupQueryFn = function () {
        if (!$('#query').val()) {
            movieRama.app.clearAppState('');
            movieRama.tomatoService.setQueryParameter('');
            movieRama.app.init();
        }
    }

    app.clickExpandFn = function () {
        var element = $(this).closest('.movie-container');
        var isExpanted = !!$(element).data("expanted");
        var hasData = !!$(element).data("hasdata");
        if (!isExpanted) {
            if (!hasData) {
                movieRama.tomatoService.getMovieDetails(element);
            }
            else {
                movieRama.dom.showMovieDetails(element);
            }
        }
        else {
            movieRama.dom.hideMovieDetails(element);
        }
    }

    return {
        init: app.init,
        searchMovie: app.searchMovie,
        clearAppState: app.clearAppState,
        infiniteScrollFn: app.infiniteScrollFn,
        clickSearchFn: app.clickSearchFn,
        keypressQueryFn: app.keypressQueryFn,
        keyupQueryFn: app.keyupQueryFn,
        clickExpandFn: app.clickExpandFn
    }

})(movieRama.app = movieRama.app || {},jQuery, _);

(function ($) {
    $(document).ready(function () {
        //register Ajax Events to handle the spinner
        $(document).ajaxComplete(function () {
            movieRama.dom.hideSpinner();
        });

        $(document).ajaxError(function () {
            movieRama.dom.hideSpinner();
        });

        //init App
        movieRama.app.init();

        //attach infinite scrolling event
        $(window).scroll(movieRama.app.infiniteScrollFn);

        //attach search event
        $(document).on('click', '#search', movieRama.app.clickSearchFn)

        //when user press enter on searchbox
        $(document).on('keypress', '#query', movieRama.app.keypressQueryFn);

        //when user deletes the search query
        $(document).on('keyup', '#query', movieRama.app.keyupQueryFn);

        //when user clicks on a movie
        $(document).on('click', '.movie-container', movieRama.app.clickExpandFn);

        //when user clicks a movie link don't close the expanded movie
        $(document).on('click', '.movie-link', function (e) {
            e.stopPropagation();
        })

    })
})(jQuery)